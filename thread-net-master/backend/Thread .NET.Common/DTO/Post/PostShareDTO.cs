﻿using System;
using System.Collections.Generic;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Common.DTO.Post
{
    public sealed class PostShareDTO
    {
        public string Email { get; set; }

        public string Link { get; set; }
    }
}
