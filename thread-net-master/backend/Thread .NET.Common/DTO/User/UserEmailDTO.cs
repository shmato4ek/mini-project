﻿namespace Thread_.NET.Common.DTO.User
{
    public class UserEmailDTO
    {
        public string Email { get; set; }
    }
}
