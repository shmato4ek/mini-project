﻿namespace Thread_.NET.Common.DTO.User
{
    public sealed class UserResetDTO
    {
        public int Id { get; set; }
        public string Password { get; set; }
    }
}
