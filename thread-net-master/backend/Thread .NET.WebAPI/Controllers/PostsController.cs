﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;
        private readonly MailService _mailService;
        private readonly UserService _userService;

        public PostsController(PostService postService, LikeService likeService, MailService mailService, UserService userService)
        {
            _postService = postService;
            _likeService = likeService;
            _likeService = likeService;
            _mailService = mailService;
            _userService = userService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> GetPosts()
        {
            return Ok(await _postService.GetAllPosts());
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<PostDTO>> GetPost(int id)
        {
            return Ok(await _postService.GetPostById(id));
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("like")]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();
            var likedUser = await _userService.GetUserById(reaction.UserId);
            var post = await _postService.GetPostById(reaction.EntityId);

            await _likeService.LikePost(reaction);
            string actionText = reaction.IsLike ? "liked" : "disliked";
            string emailText = "Your post was " + actionText + " by " + likedUser.UserName;
            _mailService.SendEmail(emailText, post.Author.Email, post.Author.UserName);

            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> UpdatePost([FromBody] PostDTO post)
        {
            await _postService.UpdatePost(post);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            await _postService.DeletePost(id);
            return NoContent();
        }
    }
}