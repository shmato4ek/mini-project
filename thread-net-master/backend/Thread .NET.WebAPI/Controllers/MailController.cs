﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class MailController : ControllerBase
    {
        private readonly MailService _mailService;

        public MailController(MailService mailService)
        {
            _mailService = mailService;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult SharePost(PostShareDTO sharePostInfo)
        {
            _mailService.SendEmail(sharePostInfo.Link, sharePostInfo.Email);
            return Ok();
        }
    }
}