import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AuthDialogType } from "../../models/common/auth-dialog-type";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { SnackBarService } from "../../services/snack-bar.service";
import { SendDialogService } from "src/app/services/send-dialog.service";
import { environment } from "src/environments/environment";
import { SendDialogType } from "src/app/models/common/send-dialog-type";
import { ToastrService } from "ngx-toastr";
import { UserService } from "src/app/services/user.service";
import { AuthenticationService } from "src/app/services/auth.service";

@Component({
    templateUrl: "./auth-dialog.component.html",
    styleUrls: ["./auth-dialog.component.sass"],
})
export class AuthDialogComponent implements OnInit, OnDestroy {
    public dialogType = AuthDialogType;
    public userName: string;
    public password: string;
    public repeatPassword: string;
    public avatar: string;
    public email: string;

    public hidePass = true;
    public hideRepeatPass = true;
    public title: string;
    public buttonName: string;
    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<AuthDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private userService: UserService,
        private authService: AuthenticationService,
        private snackBarService: SnackBarService,
        private sendDialogService: SendDialogService,
        private toastr: ToastrService
    ) {}

    public ngOnInit() {
        this.avatar =
            "https://avatars.mds.yandex.net/get-ott/374297/2a000001616b87458162c9216ccd5144e94d/orig";
        this.title =
            this.data.dialogType === AuthDialogType.SignIn
                ? "Tell me who you are and SIGN IN"
                : this.data.dialogType === AuthDialogType.SignUp
                ? "Tell me who you are and SIGN UP"
                : "Write your new password";
        this.buttonName =
            this.data.dialogType === AuthDialogType.SignIn
                ? "Sign In"
                : this.data.dialogType === AuthDialogType.SignUp
                ? "Sign Up"
                : "Reset";
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    public signIn() {
        this.authService
            .login({ email: this.email, password: this.password })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (response) => this.dialogRef.close(response),
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public signUp() {
        this.authService
            .register({
                userName: this.userName,
                password: this.password,
                email: this.email,
                avatar: this.avatar,
            })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (response) => this.dialogRef.close(response),
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public resetPassword() {
        if (this.password !== this.repeatPassword) {
            this.toastr.error(
                "The password and repeat password fields do not match!"
            );
        } else {
            this.userService
                .resetPassword({
                    id: this.data.userId,
                    password: this.password,
                })
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (response) => {
                        this.dialogRef.close(response);
                        this.toastr.success(
                            "Password was successfully changed!"
                        )
                    },
                    (error) => this.snackBarService.showErrorMessage(error)
                );
        }
    }

    public openSendDialog() {
        this.sendDialogService.openSendDialog(
            `${environment.appUrl}/reset-password`,
            SendDialogType.ResetPassword
        );
    }
}
