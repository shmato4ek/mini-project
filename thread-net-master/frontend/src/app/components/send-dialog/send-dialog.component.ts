import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { SnackBarService } from "../../services/snack-bar.service";
import { MailService } from "src/app/services/mail.service";
import { ToastrService } from "ngx-toastr";
import { SendDialogType } from "src/app/models/common/send-dialog-type";
import { UserService } from "src/app/services/user.service";

@Component({
    templateUrl: "./send-dialog.component.html",
    styleUrls: ["./send-dialog.component.sass"],
})
export class SendDialogComponent implements OnInit, OnDestroy {
    public dialogType = SendDialogType;
    public email: string;
    public link: string;
    public title: string;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<SendDialogComponent>,
        private matDialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private mailService: MailService,
        private userService: UserService,
        private snackBarService: SnackBarService,
        private toastr: ToastrService
    ) {
        this.title =
            data.dialogType === SendDialogType.SharePost
                ? "Write the email of the person you want to send the post to"
                : "Write your email to reset password";
    }

    public ngOnInit() {
        this.link = this.data.link;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    public sendLink() {
        this.mailService
            .sendLink({ email: this.email, link: this.link })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                    this.matDialog.closeAll();
                    this.toastr.success(
                        `Link was successfully sended to ${this.email}!`
                    );
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public sendResetLink() {
        this.userService
            .getUserFromEmail(this.email)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (response) => {
                    this.link += `/${response.body.id}`;
                    this.sendLink();
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}
