import { Component, Input, OnChanges } from "@angular/core";
import { empty, Observable, Subject } from "rxjs";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import { AuthDialogType } from "src/app/models/common/auth-dialog-type";
import { Reaction } from "src/app/models/reactions/reaction";
import { User } from "src/app/models/user";
import { AuthDialogService } from "src/app/services/auth-dialog.service";
import { AuthenticationService } from "src/app/services/auth.service";
import { LikeService } from "src/app/services/like.service";
import { Comment } from "../../models/comment/comment";

@Component({
    selector: "app-comment",
    templateUrl: "./comment.component.html",
    styleUrls: ["./comment.component.sass"],
})
export class CommentComponent implements OnChanges {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Input() public deleteComment: (commentId: number) => void;
    @Input() public editComment: (
        commentId: number,
        commentBody: string
    ) => void;

    public likes: Reaction[];
    public dislikes: Reaction[];
    public isEditingMode = false;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private likeService: LikeService,
        private authDialogService: AuthDialogService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public ngOnChanges() {
        this.updateLikes();
    }

    public toggleIsEditingMode() {
        this.isEditingMode = !this.isEditingMode;
    }

    public onDeleteComment() {
        this.deleteComment(this.comment.id);
    }

    public onEditComment() {
        this.toggleIsEditingMode();
        this.editComment(this.comment.id, this.comment.body);
    }

    private updateLikes() {
        this.likes = this.comment.reactions.filter(
            (reaction) => reaction.isLike
        );
        this.dislikes = this.comment.reactions.filter(
            (reaction) => !reaction.isLike
        );
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) =>
                        this.likeService.reactComment(
                            this.comment,
                            userResp,
                            true
                        )
                    ),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => {
                    this.comment = comment as Comment;
                    this.updateLikes();
                });

            return;
        }

        this.likeService
            .reactComment(this.comment, this.currentUser, true)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => {
                this.comment = comment;
                this.updateLikes();
            });
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) =>
                        this.likeService.reactComment(
                            this.comment,
                            userResp,
                            false
                        )
                    ),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => {
                    this.comment = comment as Comment;
                    this.updateLikes();
                });

            return;
        }

        this.likeService
            .reactComment(this.comment, this.currentUser, false)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => {
                this.comment = comment;
                this.updateLikes();
            });
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(AuthDialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}
