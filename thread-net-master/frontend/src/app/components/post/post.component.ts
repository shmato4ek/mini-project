import { Component, Input, OnChanges, OnDestroy, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Post } from "../../models/post/post";
import { AuthenticationService } from "../../services/auth.service";
import { AuthDialogService } from "../../services/auth-dialog.service";
import { empty, Observable, Subject } from "rxjs";
import { AuthDialogType } from "../../models/common/auth-dialog-type";
import { LikeService } from "../../services/like.service";
import { NewComment } from "../../models/comment/new-comment";
import { CommentService } from "../../services/comment.service";
import { User } from "../../models/user";
import { Comment } from "../../models/comment/comment";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import { SnackBarService } from "../../services/snack-bar.service";
import { Reaction } from "src/app/models/reactions/reaction";
import { SendDialogService } from "src/app/services/send-dialog.service";
import { SendDialogType } from "src/app/models/common/send-dialog-type";
import { environment } from "src/environments/environment";

@Component({
    selector: "app-post",
    templateUrl: "./post.component.html",
    styleUrls: ["./post.component.sass"],
})
export class PostComponent implements OnDestroy, OnChanges, OnInit {
    @Input() public post: Post;
    @Input() public currentUser: User;
    @Input() public deletePost: (postId: number) => void;
    @Input() public editPost: (postId: number, postBody: string) => void;

    public showComments = false;
    public isEditingMode = false;
    public newComment = {} as NewComment;

    public likes: Reaction[];
    public dislikes: Reaction[];

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private sendDialogService: SendDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private toastr: ToastrService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public ngOnInit() {
        this.editComment = this.editComment.bind(this);
        this.deleteComment = this.deleteComment.bind(this);
    }

    public ngOnChanges() {
        this.updateLikes();
    }

    public openSendDialog(postId: number) {
        this.sendDialogService.openSendDialog(
            `${environment.appUrl}/thread/${postId}`,
            SendDialogType.SharePost
        );
    }

    private updateLikes() {
        this.likes = this.post.reactions.filter((reaction) => reaction.isLike);
        this.dislikes = this.post.reactions.filter(
            (reaction) => !reaction.isLike
        );
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public toggleIsEditingMode() {
        this.isEditingMode = !this.isEditingMode;
    }

    public onDeletePost() {
        this.deletePost(this.post.id);
    }

    public onEditPost() {
        this.toggleIsEditingMode();
        this.editPost(this.post.id, this.post.body);
    }

    public editComment(commentId: number, commentBody: string) {
        const commentToEdit = this.post.comments.find(
            (comment) => comment.id === commentId
        );
        this.commentService
            .editComment({ ...commentToEdit, body: commentBody })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                    this.toastr.success(
                        "Your comment was successfully edited!"
                    );
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public deleteComment(commentId: number) {
        this.commentService
            .deleteComment(commentId)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                    this.post.comments = this.post.comments.filter(
                        (comment) => comment.id !== commentId
                    );
                    this.toastr.success(
                        "Your comment was successfully deleted!"
                    );
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) =>
                        this.likeService.reactPost(this.post, userResp, true)
                    ),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => {
                    this.post = post;
                    this.updateLikes();
                });

            return;
        }

        this.likeService
            .reactPost(this.post, this.currentUser, true)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post;
                this.updateLikes();
            });
    }

    public dislikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) =>
                        this.likeService.reactPost(this.post, userResp, false)
                    ),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => {
                    this.post = post;
                    this.updateLikes();
                });

            return;
        }

        this.likeService
            .reactPost(this.post, this.currentUser, false)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post;
                this.updateLikes();
            });
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(
                            this.post.comments.concat(resp.body)
                        );
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(AuthDialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort(
            (a, b) => +new Date(b.createdAt) - +new Date(a.createdAt)
        );
    }
}
