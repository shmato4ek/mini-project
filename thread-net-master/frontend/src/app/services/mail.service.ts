import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";
import { Post } from "../models/post/post";
import { ISendLink } from "../models/mail/send-link";

@Injectable({ providedIn: "root" })
export class MailService {
    public routePrefix = "/api/mail";

    constructor(private httpService: HttpInternalService) {}

    public sendLink(sendLinkInfo: ISendLink) {
        return this.httpService.postFullRequest<Post>(
            this.routePrefix,
            sendLinkInfo
        );
    }
}
