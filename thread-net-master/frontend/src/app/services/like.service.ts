import { Injectable } from "@angular/core";
import { AuthenticationService } from "./auth.service";
import { Post } from "../models/post/post";
import { NewReaction } from "../models/reactions/newReaction";
import { PostService } from "./post.service";
import { map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { User } from "../models/user";
import { Reaction } from "../models/reactions/reaction";
import { Comment } from "../models/comment/comment";
import { CommentService } from "./comment.service";

@Injectable({ providedIn: "root" })
export class LikeService {
    public constructor(
        private postService: PostService,
        private commentService: CommentService
    ) {}

    private checkHasReaction(
        reactions: Reaction[],
        currentUser: User,
        isLike: boolean
    ) {
        const hasReaction = reactions.some((x) => x.user.id === currentUser.id);
        const hasSuchReaction = reactions.some(
            (x) => x.user.id === currentUser.id && x.isLike === isLike
        );
        return [hasReaction, hasSuchReaction];
    }

    private getUpdatedReactions(
        hasReaction: boolean,
        hasSuchReaction: boolean,
        reactions: Reaction[],
        currentUser: User,
        isLike: boolean
    ) {
        let updatedReactions: Reaction[];
        if (hasReaction) {
            updatedReactions = reactions.filter(
                (x) => x.user.id !== currentUser.id
            );
            if (!hasSuchReaction) {
                updatedReactions = updatedReactions.concat({
                    isLike,
                    user: currentUser,
                });
            }
        } else {
            updatedReactions = reactions.concat({
                isLike,
                user: currentUser,
            });
        }
        return updatedReactions;
    }

    public reactPost(post: Post, currentUser: User, isLike: boolean) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike,
            userId: currentUser.id,
        };

        const [hasReaction, hasSuchReaction] = this.checkHasReaction(
            innerPost.reactions,
            currentUser,
            isLike
        );

        innerPost.reactions = this.getUpdatedReactions(
            hasReaction,
            hasSuchReaction,
            innerPost.reactions,
            currentUser,
            isLike
        );
        const [updHasReaction, updHasSuchReaction] = this.checkHasReaction(
            innerPost.reactions,
            currentUser,
            isLike
        );

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                innerPost.reactions = this.getUpdatedReactions(
                    updHasReaction,
                    updHasSuchReaction,
                    innerPost.reactions,
                    currentUser,
                    isLike
                );

                return of(innerPost);
            })
        );
    }

    public reactComment(comment: Comment, currentUser: User, isLike: boolean) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike,
            userId: currentUser.id,
        };

        const [hasReaction, hasSuchReaction] = this.checkHasReaction(
            innerComment.reactions,
            currentUser,
            isLike
        );

        innerComment.reactions = this.getUpdatedReactions(
            hasReaction,
            hasSuchReaction,
            innerComment.reactions,
            currentUser,
            isLike
        );
        const [updHasReaction, updHasSuchReaction] = this.checkHasReaction(
            innerComment.reactions,
            currentUser,
            isLike
        );

        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = this.getUpdatedReactions(
                    updHasReaction,
                    updHasSuchReaction,
                    innerComment.reactions,
                    currentUser,
                    isLike
                );

                return of(innerComment);
            })
        );
    }
}
