import { Injectable } from "@angular/core";
import { HttpInternalService } from "../services/http-internal.service";
import { User } from "../models/user";
import { UserResetDto } from "../models/auth/user-reset-dto";

@Injectable({ providedIn: "root" })
export class UserService {
    public routePrefix = "/api/users";

    constructor(private httpService: HttpInternalService) {}

    public getUserFromToken() {
        return this.httpService.getFullRequest<User>(
            `${this.routePrefix}/fromToken`
        );
    }

    public getUserFromEmail(email: string) {
        return this.httpService.postFullRequest<User>(
            `${this.routePrefix}/fromEmail`,
            { email }
        );
    }

    public getUserById(id: number) {
        return this.httpService.getFullRequest<User>(`${this.routePrefix}`, {
            id,
        });
    }

    public updateUser(user: User) {
        return this.httpService.putFullRequest<void>(
            `${this.routePrefix}`,
            user
        );
    }

    public resetPassword(user: UserResetDto) {
        return this.httpService.putFullRequest<void>(
            `${this.routePrefix}/reset`,
            user
        );
    }

    public copyUser({ avatar, email, userName, id }: User) {
        return {
            avatar,
            email,
            userName,
            id,
        };
    }
}
