import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";
import { Post } from "../models/post/post";
import { NewReaction } from "../models/reactions/newReaction";
import { NewPost } from "../models/post/new-post";

@Injectable({ providedIn: "root" })
export class PostService {
    public routePrefix = "/api/posts";

    constructor(private httpService: HttpInternalService) {}

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public getPost(postId: number) {
        return this.httpService.getFullRequest<Post>(
            `${this.routePrefix}/${postId}`
        );
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(
            `${this.routePrefix}`,
            post
        );
    }

    public editPost(post: Post) {
        return this.httpService.putFullRequest<void>(this.routePrefix, post);
    }

    public deletePost(postId: number) {
        return this.httpService.deleteFullRequest<void>(
            `${this.routePrefix}/${postId}`
        );
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(
            `${this.routePrefix}/like`,
            reaction
        );
    }
}
