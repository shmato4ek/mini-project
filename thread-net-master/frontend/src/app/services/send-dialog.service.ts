import { Injectable, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Subject } from "rxjs";
import { SendDialogComponent } from "../components/send-dialog/send-dialog.component";
import { SendDialogType } from "../models/common/send-dialog-type";

@Injectable({ providedIn: "root" })
export class SendDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) {}

    public openSendDialog(link: string, dialogType: SendDialogType) {
        this.dialog.open(SendDialogComponent, {
            data: { link, dialogType },
            minWidth: 300,
            autoFocus: true,
            backdropClass: "dialog-backdrop",
            position: {
                top: "500",
            },
        });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
