export enum AuthDialogType {
    SignIn = 0,
    SignUp,
    ResetPassword
}
