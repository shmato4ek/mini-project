export interface UserResetDto {
    id: string;
    password: string;
}
